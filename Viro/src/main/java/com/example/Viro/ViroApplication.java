package com.example.Viro;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ViroApplication {

	public static void main(String[] args) {
		SpringApplication.run(ViroApplication.class, args);
	}

}
